
=== flatpak clocknlc sandbox example ===
#Note uses qt6

=== to setup flatpak enviroment ===
sudo apt install flatpak
sudo apt install gnome-software-plugin-flatpak
flatpak remote-add --if-not-exists flathub https://dl.flathub.org/repo/flathub.flatpakrepo
flatpak install flathub org.flatpak.Builder
sudo apt install flatpak-builder

=== to install flatpak platform and sdk for clocknlc enviroment  ===
flatpak install flathub org.kde.Platform//6.7 org.kde.Sdk//6.7

=== build clocknlc ===

git clone https://gitlab.com/nolimitconnectapps/nlc-sandbox-flatpak.git
cd ~/nlc-sandbox-flatpak

to use local sources change sources of com.nolimitconnect.NoLimitConnect.yaml
   sources:
      - type: dir
        path: ./

flatpak-builder build-dir com.nolimitconnect.ClockNlc.yml --force-clean
flatpak-builder --user --install --force-clean build-dir com.nolimitconnect.ClockNlc.yml
flatpak-builder --repo=myrepo --force-clean build-dir com.nolimitconnect.ClockNlc.yml
flatpak --user remote-add --no-gpg-verify myrepo myrepo
flatpak run com.nolimitconnect.ClockNlc



